"use strict";

let buttons = document.getElementsByClassName("btn");

function proverka() {
  for (const key in buttons) {
    if (Object.hasOwnProperty.call(buttons, key)) {
      const element = buttons[key];
      if (element.classList.contains("btn-press") === true) {
        element.classList.remove("btn-press");
      }
    }
  }
}

document.addEventListener("keydown", function (event) {
  switch (event.code) {
    case "KeyS":
      let s = document.getElementById("s");
      proverka();

      if (s.classList.contains("btn-press") === false) {
        s.classList.add("btn-press");
      } else {
        s.classList.remove("btn-press");
      }
      break;
    case "KeyO":
      let o = document.getElementById("o");
      proverka();

      if (o.classList.contains("btn-press") === true) {
        o.classList.remove("btn-press");
      } else {
        o.classList.add("btn-press");
      }
      break;
    case "Enter":
      let enter = document.getElementById("enter");
      proverka();

      if (enter.classList.contains("btn-press") === true) {
        enter.classList.remove("btn-press");
      } else {
        enter.classList.add("btn-press");
      }
      break;
    case "KeyE":
      let e = document.getElementById("e");
      proverka();

      if (e.classList.contains("btn-press") === true) {
        e.classList.remove("btn-press");
      } else {
        e.classList.add("btn-press");
      }
      break;
    case "KeyN":
      let n = document.getElementById("n");
      proverka();

      if (n.classList.contains("btn-press") === true) {
        n.classList.remove("btn-press");
      } else {
        n.classList.add("btn-press");
      }
      break;
    case "KeyZ":
      let z = document.getElementById("z");
      proverka();

      if (z.classList.contains("btn-press") === true) {
        z.classList.remove("btn-press");
      } else {
        z.classList.add("btn-press");
      }
      break;
    case "KeyL":
      let l = document.getElementById("l");
      proverka();

      if (l.classList.contains("btn-press") === true) {
        l.classList.remove("btn-press");
      } else {
        l.classList.add("btn-press");
      }
      break;
  }
});
